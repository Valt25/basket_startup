docker login -u Valt25 registry.gitlab.com

docker build -t registry.gitlab.com/valt25/basket_startup/backend:latest -f ./backend/Dockerfile ./backend/
docker build -t registry.gitlab.com/valt25/basket_startup/frontend:latest -f frontend/Dockerfile-production ./frontend/

docker push registry.gitlab.com/valt25/basket_startup/backend:latest
docker push registry.gitlab.com/valt25/basket_startup/frontend:latest

