import json
import random

from asgiref.sync import async_to_sync
from channels.consumer import SyncConsumer
from channels.generic.websocket import JsonWebsocketConsumer

from orders.models import Order


class PartnerConsumer(JsonWebsocketConsumer):
    def websocket_connect(self, event):
        self.user = self.scope['user']
        async_to_sync(self.channel_layer.group_add)(
            'events',
            self.channel_name
        )
        self.accept()

    def websocket_receive(self, event):
        pass

    def websocket_disconnect(self, event):
        self.channel_layer.group_discard('123', self.channel_name)

    def chat_message(self, event):
        """
        Called when someone has messaged our chat.
        """
        # Send a message down to the client

        self.send_json(event['text'])


class ClientConsumer(JsonWebsocketConsumer):
    def websocket_connect(self, event):
        order_id = self.scope["url_route"]["kwargs"]["order_id"]
        order = Order.objects.get(id=order_id)

        self.scope['session']['order_id'] = order.id
        self.scope['session']['shop_id'] = order.shop_id
        self.accept()

        self.channel_layer.group_send('shop-%s' % order.shop_id, '"connected"')

    def websocket_receive(self, event):
        data = json.loads(event['text'])
        print(data)
        order_id = self.scope['session']['order_id']
        latitude = float(data['lat'])
        longitude = float(data['lng'])

        order = Order.objects.get(id=order_id)
        order.latitude = latitude
        order.longitude = longitude
        order.save()
        # result = {'order_id': order_id, 'location': {'latitude': data['lat'], 'longitude': data['lng']}}
        result = {'order_id': order_id, 'location': {'latitude': latitude, 'longitude': longitude}}

        async_to_sync(self.channel_layer.group_send)('events', {
            "type": "chat.message",
            'text': result
        })

    def websocket_disconnect(self, event):
        pass

    def chat_message(self, event):
        """
        Called when someone has messaged our chat.
        """
        # Send a message down to the client

        self.send_json(event['text'])
