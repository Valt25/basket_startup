from django.conf.urls import url

from real_time_geolocation import consumers

channel_routing = [
    url(r"partner/$", consumers.PartnerConsumer),
    url(r"client/(?P<order_id>\w+)/$", consumers.ClientConsumer),
]