import hashlib

from orders.controllers.payment import proceed_payment
from secret_config import notification_secret


def proceed_yandex_payment(post_dict):
    is_valid = check_valid(post_dict)
    if is_valid:
        is_payed = post_dict['unaccepted'] == 'false'

        if is_payed:
            label = post_dict['label']
            amount = float(post_dict['withdraw_amount'])
            print(label)
            print(amount)
            proceed_payment(label, amount)
        else:
            print('unaccepted')


def check_valid(post_dict):
    input_string = '%s&%s&%s&%s&%s&%s&%s&%s&%s' % (post_dict['notification_type'],
                                                   post_dict['operation_id'],
                                                   post_dict['amount'],
                                                   post_dict['currency'],
                                                   post_dict['datetime'],
                                                   post_dict['sender'],
                                                   post_dict['codepro'],
                                                   notification_secret,
                                                   post_dict['label'])
    sha1 = hashlib.sha1(input_string.encode('utf-8')).hexdigest()
    return sha1 == post_dict['sha1_hash']
