# Generated by Django 2.1 on 2019-01-02 13:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0003_auto_20181028_1003'),
    ]

    operations = [
        migrations.AddField(
            model_name='shop',
            name='phone',
            field=models.CharField(max_length=20, null=True),
        ),
    ]
