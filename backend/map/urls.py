from django.conf.urls import url, include
from rest_framework import routers
from map.views import ShopViewSet, ProductViewSet, AdminShopView, AdminProductCreateUpdateDeleteView

router = routers.DefaultRouter()
router.register(r'shops', ShopViewSet, 'shop_view')
router.register(r'products', ProductViewSet, 'product_view')
router.register(r'admin/product', AdminProductCreateUpdateDeleteView, 'admin_shop_view')


urlpatterns = [
    url(r'^', include(router.urls), name='shop'),
    url(r'admin/shop/', AdminShopView.as_view(), name='admin_shop'),
]