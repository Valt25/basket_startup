from django.db import models

# Create your models here.


class Shop(models.Model):
    latitude = models.FloatField(null=True)
    longitude = models.FloatField(null=True)
    description = models.TextField(default='')

    phone = models.CharField(max_length=20, null=True)


class Product(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    price = models.FloatField()
    photo = models.ImageField(upload_to='map/images', null=True)
    category = models.CharField(max_length=25)

    shop = models.ForeignKey(Shop, on_delete=models.CASCADE, related_name='products')
