from drf_extra_fields.fields import Base64ImageField
from rest_framework import serializers

from map.models import Shop, Product


class ProductSerializer(serializers.ModelSerializer):
    photo = Base64ImageField()

    class Meta:
        model = Product
        fields = ('id', 'name', 'description', 'price', 'photo', 'category')

    def create(self, validated_data):
        instance = super().create(validated_data)
        instance.shop = validated_data['shop']
        instance.save()
        return instance

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        if instance.photo:
            ret['photo'] = instance.photo.url
        return ret


class ShopSerializer(serializers.ModelSerializer):
    products = ProductSerializer(many=True, read_only=True)

    class Meta:
        model = Shop
        fields = ('id', 'latitude', 'longitude', 'description', 'products', 'phone')
        read_only_fields = ('id', )



