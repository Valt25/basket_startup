from django.contrib import admin

# Register your models here.
from map.models import Product, Shop

admin.site.register(Shop)
admin.site.register(Product)
