from django.core.exceptions import ObjectDoesNotExist
from rest_framework import viewsets, permissions, mixins, generics
from rest_framework.exceptions import PermissionDenied, NotFound
from rest_framework.generics import GenericAPIView, RetrieveUpdateAPIView
from rest_framework.viewsets import GenericViewSet

from map.models import Shop, Product
from map.serializers import ShopSerializer, ProductSerializer


class ShopViewSet(viewsets.ReadOnlyModelViewSet):
    # permission_classes = (permissions.IsAuthenticated,)

    serializer_class = ShopSerializer
    queryset = Shop.objects.all()


class ProductViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """

    # permission_classes = (permissions.IsAuthenticated,)

    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    model = Product


class AdminShopView(RetrieveUpdateAPIView):
    serializer_class = ShopSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self):
        profile = self.request.user.profile
        if profile.shop:
            return profile.shop
        else:
            raise PermissionDenied('You are not shop administrator')


class AdminProductCreateUpdateDeleteView(generics.CreateAPIView,
                                         generics.UpdateAPIView,
                                         generics.DestroyAPIView,
                                         GenericViewSet):

    queryset = Shop.objects.all()
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = ProductSerializer

    def get_object(self):
        product_id = self.kwargs[self.lookup_field]
        try:
            product = Product.objects.get(id=product_id)
        except ObjectDoesNotExist:
            raise NotFound()
        if product.shop == self.request.user.profile.shop:
            return product
        else:
            raise PermissionDenied('This product is not from your shop')

    def perform_create(self, serializer):
        shop = self.request.user.profile.shop
        if shop:
            serializer.save(shop=shop)
        else:
            raise PermissionDenied('You are not shop administrator')
