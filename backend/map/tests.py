from django.contrib.auth.models import User
from django.test import TestCase

# Create your tests here.
from django.urls import reverse
from rest_framework.test import APIClient

from map.models import Shop, Product
from map.serializers import ShopSerializer
from orders.models import Basket, ProductToBuy
from profile.models import Profile

client = APIClient()


class ShopTest(TestCase):
    """ Test module for shop app"""
    shop_id_to_test = 3

    def setUp(self):
        shop_to_test = Shop.objects.create(id=self.shop_id_to_test,
            latitude=51, longitude=52, description='Some description')
        Shop.objects.create(
            latitude=53, longitude=54, description='Some another description')
        user = User.objects.create(username='For shop')
        profile = Profile.objects.create(user=user, shop=shop_to_test)

    def test_shop_list(self):
        shop_list = Shop.objects.all()
        shop_serializer = ShopSerializer(shop_list, many=True)
        response = client.get('/map/shops/')
        self.assertEqual(response.data, shop_serializer.data
                         , 'All stores have to be serialized and returned, independently from auth')

    def test_shop_retrieve(self):
        shop = Shop.objects.get(id=self.shop_id_to_test)
        shop_serializer = ShopSerializer(shop)
        response = client.get('/map/shops/%s/' % self.shop_id_to_test)
        self.assertEqual(response.data, shop_serializer.data,
                         'Store have to be serialized and returned, independently from auth')

