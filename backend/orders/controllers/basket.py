from rest_framework.exceptions import ValidationError

from map.models import Product
from orders.models import Basket, ProductToBuy, Order


def add_to_basket(basket: Basket, product: Product, amount):
    if not product.shop == basket.shop:
        raise ValidationError('This product is not belong to this shop')
    product_to_buy, created = ProductToBuy.objects.get_or_create(basket=basket, product=product)
    remind = product_to_buy.amount + amount
    if remind > 0:
        product_to_buy.amount += amount
        product_to_buy.save()
    elif remind == 0:
        product_to_buy.delete()
    else:
        raise ValidationError('There is no such number of products in basket')


def confirm_basket(basket):
    order = Order(user=basket.user, shop=basket.shop)
    order.save()
    products = basket.producttobuy_set.all()
    for product in products:
        product.basket = None
        product.order = order
        product.save()
    basket.delete()
    return order