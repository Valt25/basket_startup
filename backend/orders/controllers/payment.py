from orders.models import Order
from profile.models import Profile


def proceed_payment(label, amount):
    print(label)
    order = Order.objects.get(id=label)
    if amount >= order.total_price():
        order.payed = True
        order.save()