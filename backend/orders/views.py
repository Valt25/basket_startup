from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, get_object_or_404

# Create your views here.
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.exceptions import APIException, NotFound, PermissionDenied
from rest_framework.generics import GenericAPIView
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from map.models import Shop, Product
from orders.controllers.basket import add_to_basket, confirm_basket
from orders.models import Basket, ProductToBuy, Order
from orders.serializers import BasketSerializer, ProductToBuySerializer, OrderSerializer
from utils.yandex import proceed_yandex_payment


class BasketGet(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self, user, pk):
        shop = get_object_or_404(Shop, id=pk)
        try:
            basket = Basket.objects.get(user=user, shop=shop)
        except ObjectDoesNotExist:
            basket = Basket(user=user, shop=shop)
            basket.save()
        return basket

    def get(self, request, pk):
        user = request.user
        basket = self.get_object(user, pk)
        basket_serializer = BasketSerializer(basket)
        return Response(basket_serializer.data)


class PutInBasket(APIView):

    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self, user, pk):
        shop = get_object_or_404(Shop, id=pk)
        try:
            basket = Basket.objects.get(user=user, shop=shop)
        except ObjectDoesNotExist:
            basket = Basket(user=user, shop=shop)
            basket.save()
        return basket

    def post(self, request, pk):
        user = request.user
        basket = self.get_object(user, pk)
        data = self.request.data
        product = get_object_or_404(Product, id=data.get('product_id'))
        amount = int(data.get('amount'))
        add_to_basket(basket, product, amount)
        return Response(status=200)


class ConfirmBasket(APIView):

    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self, user, pk):
        shop = get_object_or_404(Shop, id=pk)
        try:
            basket = Basket.objects.get(user=user, shop=shop)
        except ObjectDoesNotExist:
            raise NotFound()

        return basket

    def get(self, request, pk):
        user = request.user
        basket = self.get_object(user, pk)
        new_order = confirm_basket(basket)
        return Response(status=200, data=new_order.id)


class AdminOrderView(GenericAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = OrderSerializer

    def get(self, request, pk):
        profile = self.request.user.profile
        if profile.shop is not None:
            order = profile.shop.order_set.filter(id=pk).first()

            if order:
                return Response(OrderSerializer(order).data)
            else:
                raise NotFound()
        else:
            raise PermissionDenied('You are not shop administrator')


class ClientOrderView(GenericAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = OrderSerializer

    def get(self, request, pk):
        order = get_object_or_404(Order, id=pk)
        if order.user.id == request.user.id:
            return Response(OrderSerializer(order).data)
        else:
            raise PermissionDenied('You are ownew of this order')


@api_view(['GET'])
@permission_classes((permissions.IsAuthenticated,))
def get_orders(request: Request):
    profile = request.user.profile
    if profile.shop is not None:
        orders = profile.shop.order_set.filter(delivered=False, payed=True).all()
        order_serializer = OrderSerializer(orders, many=True)
        return Response(order_serializer.data)
    else:
        raise PermissionDenied('You are not shop administrator')


@api_view(['POST'])
@permission_classes((permissions.IsAuthenticated,))
def deliver_order(request, pk):
    profile = request.user.profile
    if profile.shop is not None:
        order = profile.shop.order_set.filter(id=pk).first()

        if order:
            order.delivered = True
            order.save()
            return Response(status=200)
        else:
            raise NotFound()
    else:
        raise PermissionDenied('You are not shop administrator')


@api_view(['POST'])
@permission_classes((permissions.IsAuthenticated,))
def arrive_order(request, pk):
    profile = request.user.profile
    order = profile.user.order_set.filter(id=pk).first()
    if order is not None:
        order.arrived = True
        order.save()
        layer = get_channel_layer()
        async_to_sync(layer.group_send)('events', {
            "type": "chat.message",
            'text': {'order_id': order.id, 'arrived': True}
        })
        return Response(status=200)
    else:
        raise PermissionDenied('You have no permission for this order')



@api_view(['POST'])
@permission_classes(())
def payment_webhook(request):
    print(request.POST)
    if request.method == 'POST':
        try:
            proceed_yandex_payment(request.POST)
        except Exception as e:
            import traceback
            traceback.print_exc()
            print(e)
        return Response(status=200)
