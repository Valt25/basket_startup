from django.contrib import admin

# Register your models here.
from orders.models import Basket, ProductToBuy, Order

admin.site.register(Basket)
admin.site.register(ProductToBuy)
admin.site.register(Order)

