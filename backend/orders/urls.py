from django.conf.urls import url, include
from rest_framework import routers
from orders.views import BasketGet, PutInBasket, ConfirmBasket, get_orders, deliver_order, AdminOrderView, \
    ClientOrderView, payment_webhook, arrive_order

# router = routers.DefaultRouter()
# router.register(r'basket', BasketGet)
# router.register(r'orders', ProductToBuyViewSet)


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^basket/(?P<pk>[0-9]+)/$', BasketGet.as_view()),
    url(r'^basket/(?P<pk>[0-9]+)/add/$', PutInBasket.as_view()),
    url(r'^basket/(?P<pk>[0-9]+)/confirm/$', ConfirmBasket.as_view()),

    url(r'^shop/$', get_orders),
    url(r'^shop/(?P<pk>[0-9]+)/deliver', deliver_order),
    url(r'^shop/(?P<pk>[0-9]+)/arrive', arrive_order),

    url(r'^shop/(?P<pk>[0-9]+)', AdminOrderView.as_view()),

    url(r'^client/(?P<pk>[0-9]+)', ClientOrderView.as_view()),

    url(r'^yandex/payment/endpoint', payment_webhook),
]
