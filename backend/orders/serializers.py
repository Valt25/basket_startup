from map.serializers import ProductSerializer
from orders.models import Order, ProductToBuy, Basket
from rest_framework import serializers

from profile.serializers import ProfileSerializer


class ProductToBuySerializer(serializers.ModelSerializer):
    product = ProductSerializer(many=False, read_only=True)

    class Meta:
        model = ProductToBuy
        fields = ('product', 'amount')


class BasketSerializer(serializers.ModelSerializer):
    products = ProductToBuySerializer(many=True, read_only=True, source='producttobuy_set')

    class Meta:
        model = Basket
        fields = ('user', 'shop', 'products')


class OrderSerializer(serializers.ModelSerializer):
    products = ProductToBuySerializer(many=True, read_only=True, source='producttobuy_set')
    client = ProfileSerializer(read_only=True, source='user.profile')

    class Meta:
        model = Order
        fields = ('id', 'created', 'delivered', 'arrived', 'payed', 'products', 'shop_id', 'client', 'latitude', 'longitude')
