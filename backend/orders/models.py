from django.db import models

# Create your models here.


class Basket(models.Model):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    shop = models.ForeignKey('map.Shop', on_delete=models.CASCADE)


class ProductToBuy(models.Model):
    product = models.ForeignKey('map.Product', on_delete=models.CASCADE, related_name='products')
    amount = models.IntegerField(default=0)

    basket = models.ForeignKey('Basket', on_delete=models.CASCADE, null=True)
    order = models.ForeignKey('Order', on_delete=models.CASCADE, null=True)


class Order(models.Model):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    shop = models.ForeignKey('map.Shop', on_delete=models.CASCADE)

    created = models.DateTimeField(auto_now_add=True)
    payed = models.BooleanField(default=False)
    delivered = models.BooleanField(default=False)
    arrived = models.BooleanField(default=False)

    latitude = models.FloatField(null=True)
    longitude = models.FloatField(null=True)

    def total_price(self):
        products = self.producttobuy_set.all()
        result = 0
        for product in products:
            result += product.amount * product.product.price
        return result
