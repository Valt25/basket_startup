from django.contrib import admin
from django.urls import path, include, re_path

from backend import settings

"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.views.decorators.csrf import csrf_exempt
from rest_framework_swagger.views import get_swagger_view

from map.urls import urlpatterns as map_urls
from orders.urls import urlpatterns as orders_url
from profile.urls import urlpatterns as user_url
from rest_framework.authtoken import views as auth_view
from django.conf.urls import include
from feedback.urls import urlpatterns as feedback_urls
from django.views.static import serve

schema_view = get_swagger_view(title='Pastebin API')

urlpatterns = [
    re_path('media/(?P<path>.*)$', serve, {
        'document_root': settings.MEDIA_ROOT,
    }),

    path('admin/', admin.site.urls),
    path('map/', include(map_urls)),
    path('orders/', include(orders_url)),
    path('user/', include(user_url)),
    path(r'api-token-auth/', csrf_exempt(auth_view.obtain_auth_token)),
    path(r'swagger/', schema_view),
    path(r'feedback/', include(feedback_urls)),
    path(r'api-auth/', include('rest_framework.urls')),
]
