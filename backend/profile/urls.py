from django.urls import path

from profile.views import ProfileView, RegisterUserView, RegisterAdminView, LoginView, LogoutView

urlpatterns = [
    path(r'', ProfileView.as_view()),
    path(r'login/', LoginView.as_view()),
    path(r'logout/', LogoutView.as_view()),

    path(r'register/', RegisterUserView.as_view()),
    path(r'admin/register/', RegisterAdminView.as_view()),
]