from django.db import models

# Create your models here.


class Profile(models.Model):
    user = models.OneToOneField('auth.User', on_delete=models.CASCADE)
    shop = models.ForeignKey('map.Shop', on_delete=models.SET_NULL, null=True)

    name_surname = models.CharField(max_length=50)
    car_description = models.TextField(null=False)
    car_number = models.CharField(max_length=10)
    phone = models.CharField(max_length=20)


