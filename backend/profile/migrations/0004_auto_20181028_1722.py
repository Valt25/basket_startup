# Generated by Django 2.1 on 2018-10-28 17:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0003_auto_20181028_1634'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='car_description',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='profile',
            name='car_number',
            field=models.CharField(default='OO888O', max_length=10),
            preserve_default=False,
        ),
    ]
