# Create your views here.
from django.contrib.auth import login, logout
from rest_framework import permissions
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import AllowAny
from rest_framework.request import Request
from rest_framework.response import Response

from profile.serializers import ProfileSerializer, AdminProfileSerializer, LoginSerializer, RegisterProfileSerializer, \
    PatchProfileSerializer


class ProfileView(GenericAPIView):

    permission_classes = [permissions.IsAuthenticated]

    serializer_class = ProfileSerializer

    def get(self, request):
        """
        Method for getting user description
        :return: user in json {'username': string, 'name': string, 'email': string}
        """

        user = self.request.user
        serializer = ProfileSerializer(user.profile)
        return Response(serializer.data)

    def patch(self, request: Request):
        serializer = PatchProfileSerializer(self.request.user.profile, request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(status=200, data=serializer.data)
        else:
            return Response(serializer.errors, status=400)


class RegisterUserView(GenericAPIView):

    serializer_class = ProfileSerializer

    def post(self, request: Request):
        serializer = RegisterProfileSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            login(request, serializer.instance.user)
            return Response(status=201)
        else:
            return Response(serializer.errors, status=400)


class RegisterAdminView(GenericAPIView):

    serializer_class = AdminProfileSerializer

    def post(self, request: Request):
        user = request.user
        if user.is_authenticated:
            serializer = AdminProfileSerializer(instance=user.profile, data=request.data)
        else:
            serializer = AdminProfileSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=201)
        else:
            return Response(serializer.errors, status=400)


class LoginView(GenericAPIView):
    permission_classes = (AllowAny,)
    serializer_class = LoginSerializer

    def post(self, request):
        serializer = self.get_serializer(data=self.request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        login(request, user)
        result = ProfileSerializer(user.profile).data
        return Response(status=201, data=result)


class LogoutView(GenericAPIView):
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request):
        logout(request)
        return Response(status=200)