from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from map.models import Shop
from map.serializers import ShopSerializer
from profile.models import Profile


class BaseProfileSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(source='user.email', allow_null=True, default='',
                                   validators=[UniqueValidator(queryset=User.objects.all())])
    username = serializers.CharField(source='user.username', allow_null=True, default='',
                                     validators=[UniqueValidator(queryset=User.objects.all())])
    password = serializers.CharField(write_only=True)

    class Meta:
        model = Profile
        fields = ('username', 'name_surname', 'email', 'password', 'car_description', 'car_number', 'phone', 'shop')
        read_only_fields = ('shop',)


class ProfileSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(source='user.email', allow_null=True, default='',
                                   validators=[UniqueValidator(queryset=User.objects.all())])
    username = serializers.CharField(source='user.username', allow_null=True, default='',
                                     validators=[UniqueValidator(queryset=User.objects.all())])
    password = serializers.CharField(write_only=True)

    class Meta:
        model = Profile
        fields = ('username', 'name_surname', 'email', 'password', 'car_description', 'car_number', 'phone', 'shop')
        read_only_fields = ('shop',)

    def create(self, validated_data):
        user = validated_data.pop('user')
        new_user = User.objects.create(username=user['username'],
                                       email=user['email'], )
        new_user.set_password(validated_data.pop('password'))
        new_user.save()
        new_profile = Profile(user=new_user, **validated_data)
        new_profile.save()
        return new_profile

    #
    # def validate_email(self, value):
    #     if self.instance is None:
    #         if value is not None:
    #             return value
    #         else:
    #             raise serializers.ValidationError('This field have to be presented at creating')
    #     return value
    #
    # def validate_username(self, value):
    #     if self.instance is None:
    #         if value is not None:
    #             return value
    #         else:
    #             raise serializers.ValidationError('This field have to be presented at creating')
    #     return value
    #
    # def validate_password(self, value):
    #     if self.instance is None:
    #         if value is not None:
    #             return value
    #         else:
    #             raise serializers.ValidationError('This field have to be presented at creating')
    #     return value
    #
    # def validate_name(self, value):
    #     if self.instance is None:
    #         if value is not None:
    #             return value
    #         else:
    #             raise serializers.ValidationError('This field have to be presented at creating')
    #     return value


class RegisterProfileSerializer(ProfileSerializer):
    is_partner = serializers.BooleanField(write_only=True)

    class Meta:
        model = Profile
        fields = ('username', 'name_surname', 'email', 'password', 'phone', 'is_partner')
        read_only_fields = ('shop',)

    def create(self, validated_data):
        is_partner = validated_data.pop('is_partner', False)
        instance = super().create(validated_data)
        if is_partner:
            new_shop = Shop()
            new_shop.save()
            instance.shop = new_shop
            instance.save()
        return instance


class PatchProfileSerializer(BaseProfileSerializer):

    class Meta:
        model = Profile
        fields = ('username', 'name_surname', 'email', 'password', 'car_description', 'car_number', 'phone')
        read_only_fields = ('username', 'email')
        write_only_fields = ('password',)

    def update(self, instance, validated_data):
        instance = super().update(instance, validated_data)
        try:
            instance.user.set_password(validated_data['password'])
            instance.user.save()
        except KeyError:
            pass

        return instance


class AdminProfileSerializer(ProfileSerializer):
    shop = ShopSerializer()

    class Meta:
        model = Profile
        fields = ('username', 'name_surname', 'email', 'password', 'shop')

    def create(self, validated_data):
        instance = super().create(validated_data)

        shop_serializer = ShopSerializer(data=validated_data['shop'])
        shop_serializer.is_valid(raise_exception=True)
        shop = shop_serializer.save()
        instance.shop = shop
        instance.save()
        return instance

    def update(self, instance, validated_data):
        return super().update(instance, validated_data)


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField(required=True, max_length=50)
    password = serializers.CharField(required=True, max_length=50)

    def validate(self, attrs):
        email = attrs['username']
        password = attrs['password']

        user = authenticate(username=email, password=password)

        if user:
            attrs['user'] = user
            return attrs
        else:

            raise serializers.ValidationError("User is not authenticated")
