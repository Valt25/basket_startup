from django.contrib import admin

# Register your models here.
from django.utils.safestring import mark_safe

from feedback.models import BugReport


@admin.register(BugReport)
class BugReportAdmin(admin.ModelAdmin):
    readonly_fields = ['image_tag']

    def image_tag(self, obj):
        return mark_safe('<img src="{url}" width="{width}" height={height} />'.format(
            url=obj.image.url,
            width=obj.image.width,
            height=obj.image.height,
        ))
