from rest_framework import serializers
from drf_extra_fields.fields import Base64ImageField
from feedback.models import BugReport


class BugReportSerializer(serializers.ModelSerializer):
    image = Base64ImageField()

    class Meta:
        model = BugReport
        fields = ('title', 'description', 'image', 'contact')
