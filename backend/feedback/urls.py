from django.conf.urls import url, include
from rest_framework import routers

from feedback.views import CreateBugReportView

urlpatterns = [
    url(r'^', CreateBugReportView.as_view(), name='create_bug_report'),
]

