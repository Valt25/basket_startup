from django.shortcuts import render
from rest_framework import generics
# Create your views here.
from rest_framework.permissions import AllowAny

from feedback.models import BugReport
from feedback.serializers import BugReportSerializer


class CreateBugReportView(generics.CreateAPIView):

    permission_classes = (AllowAny, )
    queryset = BugReport.objects.all()
    serializer_class = BugReportSerializer