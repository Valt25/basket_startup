from django.db import models

# Create your models here.
from django.utils.safestring import mark_safe


class BugReport(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField(null=True)
    image = models.ImageField(upload_to='feedback/images', null=True)
    contact = models.CharField(max_length=200, null=True)

    def __str__(self):
        return '%s %s %s' % (self.title, str(self.description), str(self.contact))
