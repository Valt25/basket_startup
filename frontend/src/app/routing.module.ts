import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NotFoundComponent} from './root/components/not-found/not-found.component';


const routes: Routes = [
  {
    path: 'client',
    loadChildren: './client/client.module#ClientModule',
  },
  {
    path: 'login',
    loadChildren: './login/login.module#LoginModule',
  },
  {
    path: 'partner',
    loadChildren: './partner/partner.module#PartnerModule',
  },
  {
    path: 'register',
    loadChildren: './register/register.module#RegisterModule',
  },

  {path: '', redirectTo: 'client', pathMatch: 'full'},
  {path: '**', component: NotFoundComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})

export class AppRoutingModule {

}
