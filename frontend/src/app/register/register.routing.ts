import {RouterModule, Routes} from '@angular/router';
import {RegisterFormComponent} from './register-form/register-form.component';
import {RegisterSuccessComponent} from './register-success/register-success.component';
import {AccountExistComponent} from './account-exist/account-exist.component';
import {NgModule} from '@angular/core';

export const routes: Routes = [
  {path: '', component: RegisterFormComponent},
  {path: 'existed', component: AccountExistComponent},
  {path: 'success', component: RegisterSuccessComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterRoutingModule {
}

