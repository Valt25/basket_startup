import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {AppUser, RegisterUser, User} from '../../models/User';
import {AuthService} from '../../root/services/auth/auth.service';
import {generateErrorMessage, validateRePassword} from '../../root/services/utils';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent implements OnInit, OnDestroy {
  user: User;

  $appUser: Observable<AppUser>;
  userSubscription;
  regForm: FormGroup;
  as: string;
  constructor(private authService: AuthService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  userRegisterFieldGroup = [{
    name: 'Личные данные',
    fields: [
      {
        name: 'name_surname',
        plainName: 'ФИО',
        placeholder: 'ФИО',
        validators: [Validators.required, Validators.minLength(7)],
        inputType: 'text',
        default: ''
      },
      {
        name: 'email',
        plainName: 'почта',
        placeholder: 'Почта',
        validators: [Validators.required, Validators.minLength(5)],

        inputType: 'email',
        default: ''
      },
      {
        name: 'phone',
        plainName: 'телефон',
        placeholder: 'Телефон',
        validators: [Validators.required, Validators.pattern('^((\\+7|7|8)+([0-9]){10})$')],
        errorText: (errors) => {
          if (errors.pattern) {
            return 'Ожидаемый формат +71234567890'
          }
        },
        inputType: 'telephone',
        default: ''
      },
      {
        name: 'username',
        plainName: 'имя пользователя',
        placeholder: 'Username',
        validators: [Validators.required, Validators.minLength(4)],
        inputType: 'text',
        default: ''
      },
      {
        name: 'password',
        plainName: 'пароль',
        placeholder: 'Пароль',
        validators: [Validators.minLength(3), Validators.required],
        errorText: (errors) => {
          if (errors.passDontMatch) {
            return 'Пароли не совпадают'
          }
        },
        inputType: 'password',
        default: ''
      },
      {
        name: 'rePassword',
        plainName: 'пароль',
        placeholder: 'Повторите пароль',
        validators: [Validators.minLength(3), Validators.required, validateRePassword],
        errorText: (errors) => {
          if (errors.passDontMatch) {
            return 'Пароли не совпадают'
          }
        },
        inputType: 'password',
        default: ''
      },
    ]
  },];


  ngOnInit() {
    this.user = new User();
    this.authService.getUser();
    this.$appUser = this.authService.user;
    this.as = this.route.snapshot.queryParamMap.get('as');
    this.userSubscription = this.$appUser.subscribe((user: AppUser) => {
      if (user) {
        this.router.navigate(['/register/existed'], {queryParams: {as: this.as}});
      }
    });
    this.regForm = new FormGroup(
      this.userRegisterFieldGroup.reduce((result, current) => {
        current.fields.reduce((form, field) => {
          form[field.name] = new FormControl(field.default, field.validators);
          return form

        }, result);
        return result
      }, {})
    );
  }

  register() {
    if (this.regForm.valid) {
      let result = <RegisterUser>this.userRegisterFieldGroup.reduce((result, group) => {
        group.fields.reduce((form, field) => {
          form[field.name] = this.regForm.get(field.name).value;
          return form;
        }, result);
        return result;
      }, {});
      let isPartner = this.as === 'partner';
      this.authService.register(result, isPartner).subscribe((errors) => {
        if (errors) {
          let uniqueText = 'This field must be unique.';
          if (errors.username) {
            if (errors.username.indexOf(uniqueText) > -1) {
              this.regForm.get('username').setErrors({uniqueError: true})
            }
          }
          if (errors.email) {
            if (errors.email.indexOf(uniqueText) > -1) {
              this.regForm.get('email').setErrors({uniqueError: true})
            }
          }

        }
      })
    } else {
      this.regForm.markAllAsTouched()
    }
  }


  generateErrorMessage(errors, field) {
    return generateErrorMessage(errors, field)
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }

}
