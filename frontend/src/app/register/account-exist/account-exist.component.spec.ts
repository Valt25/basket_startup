import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AccountExistComponent} from './account-exist.component';
import {AuthService} from '../../root/services/auth/auth.service';
import {MaterialModule} from '../../material.module';
import {RouterTestingModule} from '@angular/router/testing';
import {BehaviorSubject} from 'rxjs';
import {AppUser} from '../../models/User';

describe('AccountExistComponent', () => {
  let component: AccountExistComponent;
  let fixture: ComponentFixture<AccountExistComponent>;
  let authService: Partial<AuthService>;
  authService = {
    user: new BehaviorSubject(new AppUser({}))
  }
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountExistComponent ],
      imports: [MaterialModule, RouterTestingModule],
      providers: [
        {provide: AuthService, useValue: authService}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountExistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
