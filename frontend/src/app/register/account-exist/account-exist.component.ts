import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../root/services/auth/auth.service';
import {AppUser} from '../../models/User';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-account-exist',
  templateUrl: './account-exist.component.html',
  styleUrls: ['./account-exist.component.scss']
})
export class AccountExistComponent implements OnInit {


  private userSubscription;

  as: string;

  isSameLevel: boolean;
  isLowLevel: boolean;
  isHighLevel: boolean;

  constructor(private authService: AuthService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.as = this.route.snapshot.queryParamMap.get('as');
    this.userSubscription = this.authService.user.subscribe((user) => {
      if (user !== undefined) {
        if (user !== null) {
          this.processUserAndFrom(user, this.as);
        } else {
          this.router.navigate(['/register'], {queryParams: {as: this.as}});
        }
      }
    })
  }

  private processUserAndFrom(user: AppUser, as: string) {
    let registerAsPartner = as === 'partner';

    this.isSameLevel = (user.shop && registerAsPartner) || (!user.shop && !registerAsPartner);
    this.isLowLevel = user.shop && !registerAsPartner;
    this.isHighLevel = !user.shop && registerAsPartner;
  }

  cancel() {
    this.router.navigate(['/'])
  }

  createNew() {
    this.authService.logout().subscribe(() => {
      this.router.navigate(['/register'], {queryParams: {as: this.as}})
    })
  }

  useExised() {
    this.router.navigate(['/'])
  }

}
