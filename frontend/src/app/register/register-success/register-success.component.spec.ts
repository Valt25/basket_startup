import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RegisterSuccessComponent} from './register-success.component';
import {MaterialModule} from '../../material.module';
import {RouterTestingModule} from '@angular/router/testing';
import {AuthService} from '../../root/services/auth/auth.service';
import {BehaviorSubject} from 'rxjs';
import {AppUser} from '../../models/User';

describe('RegisterSuccessComponent', () => {
  let component: RegisterSuccessComponent;
  let fixture: ComponentFixture<RegisterSuccessComponent>;
  let authService: Partial<AuthService>;

  authService = {
    user: new BehaviorSubject(new AppUser({}))
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterSuccessComponent, ],
      imports: [
        MaterialModule,
        RouterTestingModule,
      ],
      providers: [
        { provide: AuthService, useValue: authService }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
