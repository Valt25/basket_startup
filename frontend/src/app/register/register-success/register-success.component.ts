import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../root/services/auth/auth.service';
import {AppUser} from '../../models/User';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register-success',
  templateUrl: './register-success.component.html',
  styleUrls: ['./register-success.component.scss']
})
export class RegisterSuccessComponent implements OnInit {

  constructor(private authService: AuthService,
              private router: Router) { }

  isAdmin: boolean;

  ngOnInit() {
    this.authService.user.subscribe((user: AppUser) => {
      if (user) {
        this.isAdmin = !!user.shop;
        setTimeout(() => {
          this.toFillFields();
        }, 10_000);

      }

    })
  }

  toFillFields() {
    if (this.isAdmin) {
      this.router.navigate(['/partner/shop/info'], {queryParams: {first: 1}})
    } else {
      this.router.navigate(['/client/profile'], {queryParams: {first: 1}})
    }
  }

  skip() {
    if (this.isAdmin) {
      this.router.navigate(['/partner/'], {queryParams: {first: 1}})
    } else {
      this.router.navigate(['/client/'])
    }
  }

}
