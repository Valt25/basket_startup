import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RegisterRoutingModule} from './register.routing';
import {RegisterFormComponent} from './register-form/register-form.component';
import {AccountExistComponent} from './account-exist/account-exist.component';
import {RegisterSuccessComponent} from './register-success/register-success.component';
import {MaterialModule} from '../material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [RegisterFormComponent, AccountExistComponent, RegisterSuccessComponent],
  imports: [
    CommonModule,
    RegisterRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class RegisterModule { }
