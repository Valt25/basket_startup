import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {catchError, map} from 'rxjs/internal/operators';
import {BehaviorSubject, Observable, of, throwError} from 'rxjs';
import {AppUser, RegisterUser, User} from '../../../models/User';

@Injectable()
export class AuthService {
  private _user: BehaviorSubject<AppUser> = new BehaviorSubject(undefined);
  private last_user_check: Date;
  private user_check_runs = false;
  public readonly user: Observable<AppUser> = this._user.asObservable();

  constructor(private httpClient: HttpClient,
              private router: Router,
  ) {
  }

  // public getToken(): string {
  //   return localStorage.getItem('token');
  // }

  public login(user: User): Observable<any> {
    return this.httpClient.post<any>('api/user/login/', {'username': user.username, 'password': user.password}).pipe(
      catchError(err => {
        console.log(err);
        return of(err);
      }),
      map((data: any) => {
        if (data.status === 400) {
          return data.error;
        } else {
          if (data.status === undefined) {
            const user = new AppUser(data);
            this._user.next(user);
            const url_before_login = window.localStorage.getItem('urlBeforeLogin');
            if (url_before_login) {
              this.router.navigateByUrl(url_before_login);
            } else {
              this.router.navigate(['/']);
            }
            return null;
          }
        }
      })
    );
  }

  public getUser(forse = false): void {
    const now = new Date();
    if (!this.last_user_check || Math.floor((((now.getTime() - this.last_user_check.getTime()) / 1000))) > 60 || forse) {
      if (!this.user_check_runs) {
        this.user_check_runs = true;
        this.httpClient.get<AppUser>('api/user/')
          .pipe(
            catchError((e) => {
              this._user.next(null);
              return throwError(e)
            })
          ).subscribe((res) => {

          const user = new AppUser(res);
          this._user.next(user);
          this.last_user_check = now;
          this.user_check_runs = false;
        })
      }
    }
  }

  public patchUser(user: AppUser): Observable<AppUser> {
    return this.httpClient.patch<AppUser>('api/user/', user);
  }

  public logout(): Observable<any> {
    return this.httpClient.post<any>('api/user/logout/', {}).pipe(map((res) => {
      console.log(res);
      this._user.next(undefined);
    }));
  }

  public register(newUser: RegisterUser, isPartner: boolean): Observable<any> {
    return this.httpClient.post('api/user/register/', {...newUser, is_partner: isPartner}).pipe(
      catchError(err => {
        console.log(err);
        return of(err);
      }),
      map((data: any) => {
        if (data) {
          if (data.status === 400) {
            return data.error;
          }
        } else {
          this.router.navigate(['/register/success']);
        }
      })
    );
  }

  public registerPartner(newUser: RegisterUser): Observable<any> {
    return this.httpClient.post('api/user/admin/register/', newUser).pipe(
      catchError(err => {
        console.log(err);
        return of(err);
      }),
      map((data: any) => {
        if (data) {
          if (data.status === 400) {
            return data.error;
          }
        } else {
          this._user.subscribe((user: AppUser) => {
            if (user !== undefined) {
              this.router.navigate(['/partner']);

            } else {
              this.router.navigate(['/login']);
            }
          })
        }
      })
    );
  }
}
