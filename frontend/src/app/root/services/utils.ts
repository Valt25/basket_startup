import {FormControl} from '@angular/forms';

function radians(degrees: number): number {
  return degrees * Math.PI / 180;
}

export function getWsProtocol() {
  if (location.protocol === 'https:') {
    return 'wss://';
  } else {
    return 'ws://';
  }
}

export function distance(first, second): number {
    if (first && second) {
      if (first.latitude && first.longitude && second.latitude && second.longitude) {
        const slat = radians(first.latitude);
        const slon = radians(first.longitude);
        const elat = radians(second.latitude);
        const elon = radians(second.longitude);

        return 6371.01 * Math.acos(Math.sin(slat) * Math.sin(elat) + Math.cos(slat) * Math.cos(elat) * Math.cos(slon - elon));
      }
    }
    return NaN;
}

export function validateRePassword(control: FormControl){
  const { root } = control;
  const pass = root.get('password'),
        rePass = root.get('rePassword');
  if(!pass || !rePass) {
    return null;
  }

  const passVal = pass.value,
        rePassVal = rePass.value;
  if (passVal===rePassVal) {
    // pass.updateValueAndValidity();
    return null;
  } else {
    return { passDontMatch: true }
    return passVal == rePassVal ? null : {passDontMatch: true}
  }
}

export function generateErrorMessage(errors, field) {
  let name = field.plainName;
  if (errors.required) {
    return `Требуется ${name}`
  }
  if (errors.minlength) {
    return `Значение не менее ${errors.minlength.requiredLength} знаков`;
  }
  if (errors.uniqueError) {
    return 'Это значение уже занято'
  }
  if (field.errorText) {
    return field.errorText(errors);
  }

}
