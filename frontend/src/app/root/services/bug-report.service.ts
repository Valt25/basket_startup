import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BugReport} from '../../models/BugReport';

@Injectable({
  providedIn: 'root'
})
export class BugReportService {

  constructor(private http: HttpClient) {
  }

  sendBugReport(bugReport: BugReport): Observable<any> {
    return this.http.post<any>('api/feedback/', bugReport);
  }
}
