import {inject, TestBed} from '@angular/core/testing';

import {BugReportService} from './bug-report.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('BugReportService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        BugReportService,
      ]
    });
  });

  it('should be created', inject([BugReportService], (service: BugReportService) => {
    expect(service).toBeTruthy();
  }));
});
