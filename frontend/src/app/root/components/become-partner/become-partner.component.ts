import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {AppUser} from '../../../models/User';

@Component({
  selector: 'app-become-partner',
  templateUrl: './become-partner.component.html',
  styleUrls: ['./become-partner.component.scss']
})
export class BecomePartnerComponent implements OnInit, OnDestroy {

  @Input() is_in_sidenav: boolean;

  constructor(private authService: AuthService, private router: Router) {
  }
  $appUser: Observable<AppUser>;
  showProposal: boolean;
  showToPartner: boolean;
  private userSubscription;

  ngOnInit() {
    this.authService.getUser();
    this.$appUser = this.authService.user;
    this.userSubscription = this.$appUser.subscribe((user) => {
      this.showProposal = !user || !user.shop;
      this.showToPartner = user && !!user.shop && !this.router.url.startsWith('/partner');
    })
  }
  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }
}
