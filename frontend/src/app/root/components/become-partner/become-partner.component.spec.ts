import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BecomePartnerComponent} from './become-partner.component';
import {MaterialModule} from '../../../material.module';
import {RouterTestingModule} from '@angular/router/testing';
import {AuthService} from '../../services/auth/auth.service';
import {BehaviorSubject} from 'rxjs';
import {AppUser} from '../../../models/User';

describe('BecomePartnerComponent', () => {
  let component: BecomePartnerComponent;
  let fixture: ComponentFixture<BecomePartnerComponent>;
  let authService: Partial<AuthService>;

  authService = {
    user: new BehaviorSubject(new AppUser({})),
    getUser(forse: boolean = false): void {
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BecomePartnerComponent ],
      imports: [
        MaterialModule,
        RouterTestingModule
      ],
      providers: [
        {provide: AuthService, useValue: authService}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BecomePartnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
