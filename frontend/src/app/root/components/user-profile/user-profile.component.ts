import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {AppUser} from '../../../models/User';
import {AuthService} from '../../services/auth/auth.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit, OnDestroy{
  @Input() is_in_sidenav: boolean;

  constructor(private authService: AuthService,
              private router: Router) {
  }
  $appUser: Observable<AppUser>;
  is_logged_in: boolean;
  name_surname: string;
  private userSubscription;

  ngOnInit() {
    this.authService.getUser();
    this.$appUser = this.authService.user;
    this.userSubscription = this.$appUser.subscribe((user) => {
      if (user) {
        if (user.shop && this.router.url === '/') {
          this.router.navigate(['/partner'])
        }
        this.is_logged_in = true;
        this.name_surname = user.name_surname
      } else {
        this.is_logged_in = false
      }
    })
  }

   logout() {
    this.authService.logout().subscribe(() => {
          this.router.navigate(['/client']);
    });
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }
}
