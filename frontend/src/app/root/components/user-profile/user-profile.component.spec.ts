import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UserProfileComponent} from './user-profile.component';
import {AuthService} from '../../services/auth/auth.service';
import {BehaviorSubject} from 'rxjs';
import {MaterialModule} from '../../../material.module';
import {RouterTestingModule} from '@angular/router/testing';

describe('UserProfileComponent', () => {
  let component: UserProfileComponent;
  let fixture: ComponentFixture<UserProfileComponent>;
  let authService: Partial<AuthService>;

  authService = {
    user: new BehaviorSubject(null),
    getUser(forse: boolean = false): void {
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserProfileComponent ],
      imports: [
        MaterialModule,
        RouterTestingModule.withRoutes([])
      ],
      providers: [
        {provide: AuthService, useValue: authService}
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
