import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BugCollectorComponent} from './bug-collector.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Component} from '@angular/core';
import {BugReportService} from '../../services/bug-report.service';

@Component({selector: 'app-modal', template: ''})
class ModalStubComponent {}

describe('BugCollectorComponent', () => {
  let component: BugCollectorComponent;
  let fixture: ComponentFixture<BugCollectorComponent>;
  let bugReportStubService: Partial<BugReportService>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        BugCollectorComponent,
        ModalStubComponent
      ],
      providers: [
        {provide: BugReportService, useValue: bugReportStubService}
      ],
      imports: [FormsModule, ReactiveFormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BugCollectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
