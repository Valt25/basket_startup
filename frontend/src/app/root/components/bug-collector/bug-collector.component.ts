import {ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {BugReportService} from '../../services/bug-report.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BugReport} from '../../../models/BugReport';
import {ModalService} from '../../../shared/modal.service';

@Component({
  selector: 'app-bug-collector',
  templateUrl: './bug-collector.component.html',
  styleUrls: ['./bug-collector.component.scss']
})
export class BugCollectorComponent implements OnInit {
  @Input() is_in_sidenav: boolean;

  constructor(private modalService: ModalService, private cd: ChangeDetectorRef, private bugReportService: BugReportService) {
  }

  reportForm: FormGroup;
  bugReport = new BugReport('', null, null, null);

  ngOnInit() {
    this.reportForm = new FormGroup({
      'title': new FormControl(this.bugReport.title, [Validators.required, Validators.minLength(3)]),
      'description': new FormControl(this.bugReport.description, [Validators.minLength(10)]),
      'contact': new FormControl(this.bugReport.contact),
      'image': new FormControl(this.bugReport.image),
    });
  }

  openBugModal() {
    this.modalService.open('bug-collector');
  }

  closeBugModal() {
    this.modalService.close('bug-collector');
  }

  get title() {
    return this.reportForm.get('title');
  }

  get description() {
    return this.reportForm.get('description');
  }

  get contact() {
    return this.reportForm.get('contact');
  }

  get image() {
    return this.reportForm.get('image');
  }

  onFileChange(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.reportForm.patchValue({
          image: reader.result
        });
        // need to run CD since file load runs outside of zone
        this.cd.markForCheck();
      };
    }
  }

  submitForm(): void {
    if (this.reportForm.valid) {
      this.bugReport = new BugReport(this.reportForm.get('title').value,
        this.reportForm.get('contact').value,
        this.reportForm.get('description').value,
        this.reportForm.get('image').value);
      this.bugReportService.sendBugReport(this.bugReport).subscribe((res: any) => {
        this.modalService.close('bug-collector');
        this.reportForm.reset();

      });
    }
  }


}
