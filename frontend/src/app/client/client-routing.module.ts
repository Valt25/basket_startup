import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MenuComponent} from './components/menu/menu.component';
import {BasketComponent} from './components/basket/basket.component';
import {MapComponent} from './components/map/map.component';
import {PaymentComponent} from './components/payment/payment.component';
import {MapWrapComponent} from './components/map-wrap/map-wrap.component';
import {ConfirmationComponent} from './components/confirmation/confirmation.component';
import {UserInfoComponent} from './components/user-info/user-info.component';

const routes: Routes = [
  {path: 'menu/:id', component: MenuComponent},

  {path: 'basket/:id', component: BasketComponent},

  {path: 'payment/:id', component: PaymentComponent},
  {path: 'map', component: MapComponent},
  {path: 'profile', component: UserInfoComponent},

  {path: 'navigation/:id', component: MapWrapComponent},

  {path: 'confirmation/:id', component: ConfirmationComponent},
  {path: '', redirectTo: 'map', pathMatch: 'full'},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule {
}
