import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../../root/services/auth/auth.service';
import {AppUser} from '../../../models/User';
import {generateErrorMessage} from '../../../root/services/utils';
import {ModalService} from '../../../shared/modal.service';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit, OnDestroy, AfterViewInit {
  userSubscription;
  userForm: FormGroup;

  constructor(private authService: AuthService,
              private router: Router,
              private modalService: ModalService,
              private route: ActivatedRoute) {
  }

  userRegisterFieldGroup = [
    {
      name: 'name_surname',
      plainName: 'ФИО',
      placeholder: 'ФИО',
      validators: [Validators.required, Validators.minLength(7)],
      inputType: 'text',
      disabled: false,
      default: ''
    },
    {
      name: 'email',
      plainName: 'почта',
      placeholder: 'Почта',
      validators: [Validators.required, Validators.minLength(5)],
      inputType: 'email',
      disabled: true,
      default: ''
    },
    {
      name: 'phone',
      plainName: 'телефон',
      placeholder: 'Телефон',
      validators: [Validators.required, Validators.pattern('^((\\+7|7|8)+([0-9]){10})$')],
      errorText: (errors) => {
        if (errors.pattern) {
          return 'Ожидаемый формат +71234567890'
        }
      },
      inputType: 'telephone',
      disabled: false,

      default: ''
    },
    {
      name: 'username',
      plainName: 'имя пользователя',
      placeholder: 'Username',
      validators: [Validators.required, Validators.minLength(4)],
      inputType: 'text',
      disabled: true,

      default: ''
    },
    {
      name: 'car_number',
      plainName: 'Номер автомобиля',
      placeholder: 'Номер',
      validators: [Validators.minLength(4)],
      inputType: 'text',
      disabled: false,
      default: ''
    },
    {
      name: 'car_description',
      plainName: 'Описание автомобиля',
      placeholder: 'Описание',
      validators: [Validators.minLength(4)],
      inputType: 'text',
      disabled: false,

      default: ''
    }
  ];

  ngAfterViewInit(): void {
    let first = this.route.snapshot.queryParamMap.get('first');
    if (first) {
      this.modalService.open('first-profile')
    }
  }

  ngOnInit() {

    this.userSubscription = this.authService.user.subscribe((user: AppUser) => {
      if (user !== undefined) {
        if (user !== null) {
          this.userRegisterFieldGroup.map((field) => {
            field.default = user[field.name];
            return field;
          });
          this.userForm = new FormGroup(
            this.userRegisterFieldGroup.reduce((form, field) => {
              form[field.name] = new FormControl({value: field.default, disabled: field.disabled}, field.validators);
              return form;

            }, {})
          );
        } else {
          this.router.navigate(['/login'])

        }

      }
    });
  }

  save() {
    if (this.userForm.valid) {
      let result = <AppUser>this.userRegisterFieldGroup.reduce((result, field) => {
        console.log(field);
        if (!field.disabled && this.userForm.get(field.name).value) {
          result[field.name] = this.userForm.get(field.name).value;
        }
        return result;
      }, {});
      console.log(result);
      this.authService.patchUser(result).subscribe((errors) => {
        console.log(errors)
      })
    } else {
      this.userForm.markAllAsTouched()
    }
  }

  closeModal() {
    this.modalService.close('first-profile')
  }

  generateErrorMessage(errors, field) {
    return generateErrorMessage(errors, field)
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }

}
