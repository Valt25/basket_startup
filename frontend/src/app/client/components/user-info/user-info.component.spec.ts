import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UserInfoComponent} from './user-info.component';
import {MaterialModule} from '../../../material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Component} from '@angular/core';
import {AuthService} from '../../../root/services/auth/auth.service';
import {RouterTestingModule} from '@angular/router/testing';
import {BehaviorSubject} from 'rxjs';
import {AppUser} from '../../../models/User';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

@Component({selector: 'app-modal', template: ''})
class ModalStubComponent {}

describe('UserInfoComponent', () => {
  let component: UserInfoComponent;
  let fixture: ComponentFixture<UserInfoComponent>;
  let authServiceStub: Partial<AuthService>;

  authServiceStub = {
    user: new BehaviorSubject(new AppUser({}))
  }
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserInfoComponent, ModalStubComponent ],
      imports: [
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule.withRoutes([]),
        NoopAnimationsModule
      ],
      providers: [
        {provide: AuthService, useValue: authServiceStub}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
