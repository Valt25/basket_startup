import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {interval} from 'rxjs';
import {ClientStoreService} from '../../services/store.service';
import {Order} from '../../../models/Order';
import {Store} from '../../../models/Store';
import {ClientOrderService} from '../../services/client-order.service';
import {WebsocketService} from '../../../root/websocket/websocket.service';
import {getWsProtocol} from '../../../root/services/utils';
import {UtilityService} from '../../../shared/utility.service';


class GeoLocation {
  latitude: number;
  longitude: number;


  constructor(latitude: number, longitude: number) {
    this.latitude = latitude;
    this.longitude = longitude;
  }
}


@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['../../../root/styles/map.styles.scss', './navigation.component.css']
})
export class NavigationComponent implements OnInit, OnDestroy {

  private geolocationSharing$;

  constructor(private orderService: ClientOrderService,
              private storeService: ClientStoreService,
              private wsService: WebsocketService,
              private utilityService: UtilityService) {
  }

  @Input() order: Order;
  @Input() store: Store;
  ymaps: any;
  @Output() location = new EventEmitter<GeoLocation>();

  ngOnInit() {
    this.utilityService.loadScript('https://api-maps.yandex.ru/2.1/?lang=ru_RU', 'ymaps').subscribe((ymaps: any) => {
      this.ymaps = ymaps;
      this.ymaps.ready(() => {
        // Создание экземпляра карты и его привязка к контейнеру с
        // заданным id ("map").
        const myMap = new this.ymaps.Map('map', {
          // При инициализации карты обязательно нужно указать
          // её центр и коэффициент масштабирования.
          center: [55.76, 37.64], // Москва
          zoom: 10,
          controls: [new this.ymaps.control.GeolocationControl(), 'routePanelControl', 'zoomControl']
        });


            const control = myMap.controls.get('routePanelControl');

            // Зададим координаты пункта отправления с помощью геолокации.
            control.routePanel.geolocate('from');

            control.routePanel.state.set({
              // Выключим возможность задавать пункт отправления в поле ввода.
              fromEnabled: false,
              // Адрес или координаты пункта отправления.
              from: 'Москва, Льва Толстого 16',
              // Включим возможность задавать пункт назначения в поле ввода.
              toEnabled: true,
              // Адрес или координаты пункта назначения.
              to: [this.store.latitude, this.store.longitude]
            });

          });
          this.wsService.init({url: getWsProtocol() + window.location.hostname + `/ws/client/${this.order.id}/`});
          this.geolocationSharing$ = interval(10000).subscribe(() => {

            this.ymaps.geolocation.get({provider: 'browser'}).then((result) => {
              const latitude = result.geoObjects.position[0];
              const longitude = result.geoObjects.position[1];
              this.location.emit(new GeoLocation(latitude, longitude));
              this.wsService.send('client.geolocation', {lat: latitude, lng: longitude});
            });
          });

        });

  }




  public ngOnDestroy(): void {
    if (this.geolocationSharing$) {
      this.geolocationSharing$.unsubscribe();
    }
  }
}
