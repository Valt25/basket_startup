import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NavigationComponent} from './navigation.component';
import {ClientStoreService} from '../../services/store.service';
import {ClientOrderService} from '../../services/client-order.service';
import {WebsocketService} from '../../../root/websocket/websocket.service';
import {UtilityService} from '../../../shared/utility.service';
import {Observable, of} from 'rxjs';
import {WebSocketConfig} from '../../../root/websocket/websocket.interfaces';
import {Order} from '../../../models/Order';

describe('NavigationComponent', () => {
  let component: NavigationComponent;
  let fixture: ComponentFixture<NavigationComponent>;
  let storeServiceStub: Partial<ClientStoreService>;
  let orderServiceStub: Partial<ClientOrderService>;
  let wsServiceStub: Partial<WebsocketService>;
  let utilityStub: Partial<UtilityService>;

  utilityStub = {
    loadScript(scriptSource: string, objectName: string): Observable<any> {
      return of({ready: () => {console.log('ready called')}})
    }
  };

  wsServiceStub = {
    init(wsConfig: WebSocketConfig) {
    }
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigationComponent ],
      providers: [
        { provide: ClientStoreService, useValue: storeServiceStub},
        { provide: ClientOrderService, useValue: orderServiceStub},
        { provide: WebsocketService, useValue: wsServiceStub},
        { provide: UtilityService, useValue: utilityStub}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    component.order = <Order>{id: 1};
    expect(component).toBeTruthy();
  });
});
