import {Component, Input, OnInit} from '@angular/core';
import {ClientOrderService} from '../../services/client-order.service';
import {Store} from '../../../models/Store';
import {GeoLocation} from '../../../models/GeoLocation';
import {Order} from '../../../models/Order';
import {distance} from '../../../root/services/utils';

@Component({
  selector: 'app-position-status',
  templateUrl: './position-status.component.html',
  styleUrls: ['./position-status.component.scss']
})
export class PositionStatusComponent implements OnInit {

  constructor(private orderService: ClientOrderService) {
  }

  @Input() position: GeoLocation;
  @Input() store: Store;
  @Input() order: Order;

  ngOnInit() {
  }



  distanceToShop(): number {
    const res = distance(this.position, this.store);
    if (isNaN(res)) {
      return 10000;
    }
    return res;

  }


  getDistanceToShopString(): string {
    const res = distance(this.position, this.store);
    if (isNaN(res)) {
      return 'Местоположение неопределено';
    } else {
      return (res).toFixed(2) + ' км.';
    }

  }

  arriveAtShop(): void {
    this.order.arrived = true;
    this.orderService.arriveAtShopOrder(String(this.order.id)).subscribe(res => console.log(res));
  }

}
