import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PositionStatusComponent} from './position-status.component';
import {ClientOrderService} from '../../services/client-order.service';
import {testOrder} from '../payment/payment.component.spec';

describe('PositionStatusComponent', () => {
  let component: PositionStatusComponent;
  let fixture: ComponentFixture<PositionStatusComponent>;
  let orderServiceStub: Partial<ClientOrderService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PositionStatusComponent ],
      providers: [
        { provide: ClientOrderService, useValue: orderServiceStub}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PositionStatusComponent);
    component = fixture.componentInstance;
    component.order = testOrder;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
