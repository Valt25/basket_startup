import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {forkJoin} from 'rxjs';
import {BasketService} from '../../services/basket.service';
import {ClientStoreService} from '../../services/store.service';
import {ProductToBuy} from '../../../models/ProductToBuy';
import {Store} from '../../../models/Store';
import {Basket} from '../../../models/Basket';

@Component({
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],

})
export class MenuComponent implements OnInit{

  public store: Store;
  public basket: Basket;

  constructor(private route: ActivatedRoute,
              private storeService: ClientStoreService,
              private basketService: BasketService) {

  }

  ngOnInit(): void {
    console.log('213');
    this.route.paramMap.subscribe((params: ParamMap) => {
      const store$ = this.storeService.getStore(params.get('id'));
      const basket$ = this.basketService.getBasket(Number(params.get('id')));
      forkJoin(store$, basket$).subscribe(([store, basket]) => {
        this.store = store;
        const products = [];

        for (const product of basket.products) {
          products.push(product);
        }
        for (const storeProduct of store.products) {
          let productInProducts = false;

          for (const product of products) {
            if (product.product.id === storeProduct.id) {
              productInProducts = true;
              break;
            }
          }

          if (!productInProducts) {
            products.push(new ProductToBuy(storeProduct, 0));

          }
        }
      this.basket = new Basket(this.store.id, products);
      });
    });
  }

  increase(product: ProductToBuy): void {
    product.amount += 1;
    this.basketService.putInBasket(this.store.id, product.product, 1);
  }


  decrease(product: ProductToBuy): void {
    if (product.amount !== 0) {
      product.amount -= 1;

      this.basketService.putInBasket(this.store.id, product.product, -1);

    }
  }
}
