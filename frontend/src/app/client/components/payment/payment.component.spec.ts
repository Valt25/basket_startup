import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PaymentComponent} from './payment.component';
import {RouterTestingModule} from '@angular/router/testing';
import {ClientOrderService} from '../../services/client-order.service';
import {Order} from '../../../models/Order';
import {Observable, of} from 'rxjs';
import {Profile} from '../../../models/Profile';

export const testOrder = new Order(1,
  1,
  'creaeted',
  false,
  false,
  false,
  [],
  <Profile>{},
  10,
  11);
describe('PaymentComponent', () => {
  let component: PaymentComponent;
  let fixture: ComponentFixture<PaymentComponent>;
  let orderServiceStub: Partial<ClientOrderService>;

  orderServiceStub = {
    getClientOrder(id: string): Observable<Order> {
      return of(testOrder)
    }
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentComponent ],
      providers: [
        { provide: ClientOrderService, useValue: orderServiceStub},
      ],
      imports: [
        RouterTestingModule.withRoutes([]),
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
