import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MapComponent} from './map.component';
import {ClientStoreService} from '../../services/store.service';
import {Observable, of} from 'rxjs';
import {Store} from '../../../models/Store';
import {UtilityService} from '../../../shared/utility.service';

describe('MapComponent', () => {
  let component: MapComponent;
  let fixture: ComponentFixture<MapComponent>;
  let storeServiceStub: Partial<ClientStoreService>;
  let utilityStub: Partial<UtilityService>;

  utilityStub = {
    loadScript(scriptSource: string, objectName: string): Observable<any> {
      return of({
        ready: (fn: Function) => {console.log('mocked ready fn')}
      })
    }
  };

  storeServiceStub = {
    getStoreList(): Observable<Store[]> {
      return  of([new Store(), new Store()])
    }
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapComponent ],
      providers: [
        { provide: ClientStoreService, useValue: storeServiceStub},
        { provide: UtilityService, useValue: utilityStub}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
