import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MapWrapComponent} from './map-wrap.component';
import {Component} from '@angular/core';
import {ActivatedRoute, convertToParamMap} from '@angular/router';
import {Observable, of} from 'rxjs';
import {ClientOrderService} from '../../services/client-order.service';
import {ClientStoreService} from '../../services/store.service';
import {Order} from '../../../models/Order';
import {testOrder} from '../payment/payment.component.spec';
import {Store} from '../../../models/Store';

@Component({selector: 'app-position-status', template: '', inputs: ['position', 'store', 'order']})
class PositionStatusStubComponent {}

@Component({selector: 'app-navigation', template: '', inputs: ['store', 'order']})
class NavigationStubComponent {}

describe('MapWrapComponent', () => {
  let component: MapWrapComponent;
  let fixture: ComponentFixture<MapWrapComponent>;
  let orderServiceStub: Partial<ClientOrderService>;
  let storeServiceStub: Partial<ClientStoreService>;

  orderServiceStub = {
    getClientOrder(id: string): Observable<Order> {
      return of(testOrder)
    }
  };

  storeServiceStub = {
    getStore(id: string): Observable<Store> {
      return of(new Store())
    }
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MapWrapComponent,
        PositionStatusStubComponent,
        NavigationStubComponent
      ],
      providers: [
        {
          provide: ActivatedRoute, useValue: {
            paramMap: of(convertToParamMap({ id: '1' }))
          }
        },
        { provide: ClientOrderService, useValue: orderServiceStub },
        { provide: ClientStoreService, useValue: storeServiceStub}

      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapWrapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
