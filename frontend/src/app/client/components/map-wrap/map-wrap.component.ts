import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {ClientStoreService} from '../../services/store.service';
import {Order} from '../../../models/Order';
import {Store} from '../../../models/Store';
import {GeoLocation} from '../../../models/GeoLocation';
import {switchMap} from 'rxjs/operators';
import {ClientOrderService} from '../../services/client-order.service';

@Component({
  selector: 'app-map-wrap',
  templateUrl: './map-wrap.component.html',
  styleUrls: ['./map-wrap.component.scss']
})
export class MapWrapComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private orderService: ClientOrderService,
              private storeService: ClientStoreService) {
  }

  order: Order;
  store: Store;
  currentLocation: GeoLocation;


  ngOnInit() {
    this.route.paramMap.pipe(
          switchMap((params: ParamMap) => this.orderService.getClientOrder(params.get('id')))
        ).subscribe((order: Order) => {
          this.order = order;
          this.storeService.getStore(String(order.shop_id)).subscribe((store: Store) => {
            this.store = store;
          });
    });
  }

  onLocationChanges(location: GeoLocation) {
    this.currentLocation = location;
  }
}
