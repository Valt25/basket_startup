import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ClientRoutingModule} from './client-routing.module';
import {BasketComponent} from './components/basket/basket.component';
import {MenuComponent} from './components/menu/menu.component';
import {MapWrapComponent} from './components/map-wrap/map-wrap.component';
import {PositionStatusComponent} from './components/position-status/position-status.component';
import {NavigationComponent} from './components/navigation/navigation.component';
import {PaymentComponent} from './components/payment/payment.component';
import {MapComponent} from './components/map/map.component';
import {ConfirmationComponent} from './components/confirmation/confirmation.component';
import {ClientStoreService} from './services/store.service';
import {BasketService} from './services/basket.service';
import {FlexLayoutModule} from '@angular/flex-layout';
import {UserInfoComponent} from './components/user-info/user-info.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '../material.module';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    ClientRoutingModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [
    MenuComponent,
    BasketComponent,
    ConfirmationComponent,
    PaymentComponent,
    MapComponent,
    NavigationComponent,
    PositionStatusComponent,
    MapWrapComponent,
    UserInfoComponent,
  ],
  providers: [
    BasketService,
    ClientStoreService
  ]

})
export class ClientModule {
}
