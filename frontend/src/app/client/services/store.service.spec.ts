import {inject, TestBed} from '@angular/core/testing';

import {ClientStoreService} from './store.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('ClientStoreService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClientStoreService],
      imports: [HttpClientTestingModule]
    });
  });

  it('should be created', inject([ClientStoreService], (service: ClientStoreService) => {
    expect(service).toBeTruthy();
  }));
});
