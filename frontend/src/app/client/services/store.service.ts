import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Store} from '../../models/Store';

@Injectable({
  providedIn: 'root'
})
export class ClientStoreService {

  constructor(private http: HttpClient) {
  }

  getStore(id: string): Observable<Store> {
    return this.http.get<Store>(`api/map/shops/${id}/`);
  }

  getStoreList(): Observable<Store[]> {
    return this.http.get<Store[]>(`api/map/shops/`);
  }

}
