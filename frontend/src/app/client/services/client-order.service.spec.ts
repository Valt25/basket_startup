import {inject, TestBed} from '@angular/core/testing';

import {ClientOrderService} from './client-order.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('ClientOrderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClientOrderService],
      imports: [HttpClientTestingModule]
    });
  });

  it('should be created', inject([ClientOrderService], (service: ClientOrderService) => {
    expect(service).toBeTruthy();
  }));
});
