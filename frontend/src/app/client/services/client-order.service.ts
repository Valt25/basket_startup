import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/internal/operators';
import {Order} from '../../models/Order';
import {ProductToBuy} from '../../models/ProductToBuy';

@Injectable({
  providedIn: 'root'
})
export class ClientOrderService {

  constructor(private http: HttpClient) {
  }

  getClientOrder(id: string): Observable<Order> {
    return this.http.get(`api/orders/client/${id}`).pipe(
      map((plain: any) => {
        const products = [];
        for (const product of plain.products) {
          products.push(new ProductToBuy(product.product, product.amount));
        }
        return new Order(plain.id, plain.shop_id, plain.created, plain.delivered, plain.arrived, plain.payed, products, plain.client, plain.latitude, plain.longitude)
      }));
  }

  arriveAtShopOrder(id: string): Observable<any> {
    return this.http.post<Order>(`api/orders/shop/${id}/arrive`, {});
  }
}

