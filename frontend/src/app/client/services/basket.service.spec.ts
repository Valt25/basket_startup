import {inject, TestBed} from '@angular/core/testing';

import {BasketService} from './basket.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('BasketService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BasketService],
      imports: [HttpClientTestingModule]
    });
  });

  it('should be created', inject([BasketService], (service: BasketService) => {
    expect(service).toBeTruthy();
  }));
});
