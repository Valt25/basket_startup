import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Basket} from '../../models/Basket';
import {ProductToBuy} from '../../models/ProductToBuy';
import {Product} from '../../models/Product';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BasketService {

  constructor(private http: HttpClient) {
  }

  getBasket(id: number): Observable<Basket> {
    return this.http.get<Basket>(`/api/orders/basket/${id}`).pipe(
      map(basket => {
        const products = [];
        for (const product of basket.products) {
          products.push(new ProductToBuy(product.product, product.amount));
        }
        return new Basket(id, products);
      })
    );
  }

  putInBasket(shop_id: number, product: Product, amount: number): void {

    this.http.post(`api/orders/basket/${shop_id}/add/`, {'product_id': product.id, 'amount': amount})
      .subscribe(value => console.log(value));
  }

  confirmBasket(shop_id: number): Observable<Number> {
    return this.http.get<Number>(`api/orders/basket/${shop_id}/confirm/`);
  }
}

