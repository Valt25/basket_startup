import {Injectable} from '@angular/core';
import {Observable, Observer} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  constructor() { }

  public loadScript (scriptSource: string, objectName: string): Observable<any> {
  return Observable.create(function (observer: Observer<any>) {
    const node = document.createElement('script');
    node.src = scriptSource;
    node.type = 'text/javascript';
    node.async = false;
    node.charset = 'utf-8';
    document.getElementsByTagName('head')[0].appendChild(node);

    node.onload = () => {
      console.log('loaded');
      observer.next(window[objectName]);
    };
  });
}
}
