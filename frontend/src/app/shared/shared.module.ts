import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ModalComponent} from './modal/modal.component';
import {ModalService} from './modal.service';
import {UtilityService} from './utility.service';

@NgModule({
  declarations: [ModalComponent,
],
  imports: [
    CommonModule,
  ],
  providers: [
    ModalService,
    UtilityService
  ],
  exports: [
    CommonModule,
    ModalComponent
  ]
})
export class SharedModule { }
