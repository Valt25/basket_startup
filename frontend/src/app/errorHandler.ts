import * as Sentry from '@sentry/browser';
import {ErrorHandler, Injectable, isDevMode} from '@angular/core';

// Sentry.init({
//   dsn: 'https://325f5c5ddbfc4de483b2968f9afc697b@sentry.io/1421804'
// });

@Injectable()
export class SentryErrorHandler implements ErrorHandler {
  constructor() {}
  handleError(error) {
    // if (isDevMode()) {
          console.log(error);
    // }
    // Sentry.captureException(error.originalError || error);
    throw error;
  }
}

