import {NgModule} from '@angular/core';
import {PartnerMenuComponent} from './components/partnermenu/partnermenu.component';
import {PartnerOrdersComponent} from './components/partnerOrders/partnerOrders.component';
import {OrderComponent} from './components/order/order.component';
import {AdminLayoutComponent} from './layout/admin-layout/admin-layout.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NavbarComponent} from './layout/navbar/navbar.component';
import {OrderMapComponent} from './components/order-map/order-map.component';
import {CourierComponent} from './components/courier/courier.component';
import {ProductCreateComponent} from './components/product/product-create/product-create.component';
import {ProductFormComponent} from './components/product/productForm/productForm.component';
import {ProductEditComponent} from './components/product/product-edit/product-edit.component';
import {PartnerStoreService} from './services/store.service';
import {PartnerProductService} from './services/partner-product.service';
import {PartnerRoutingModule} from './partner.routing';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '../material.module';
import {ShopInfoComponent} from './components/shop-info/shop-info.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [
    PartnerMenuComponent,
    PartnerOrdersComponent,
    ProductFormComponent,
    ProductEditComponent,
    ProductCreateComponent,
    OrderComponent,
    AdminLayoutComponent,
    NavbarComponent,
    OrderMapComponent,
    CourierComponent,
    ShopInfoComponent
  ],

  imports: [
    CommonModule,
    PartnerRoutingModule,
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  exports: [AdminLayoutComponent],
  providers: [
    PartnerStoreService,
    PartnerProductService
  ]
})
export class PartnerModule { }
