import {Component, OnInit} from '@angular/core';
import {Order} from '../../../models/Order';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {switchMap} from 'rxjs/internal/operators';
import {Store} from '../../../models/Store';
import {PartnerStoreService} from '../../services/store.service';
import {PartnerOrderService} from '../../services/partner-order.service';
import {distance} from '../../../root/services/utils';

@Component({
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  order: Order;
  store: Store;

  constructor(private orderService: PartnerOrderService,
              private route: ActivatedRoute,
              private storeService: PartnerStoreService,
              private router: Router) {

  }


  ngOnInit(): void {
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) => this.orderService.getOrder(params.get('id')))
    ).subscribe((order: Order) => this.order = order);
    this.storeService.getAdminStore().subscribe((store: Store) => this.store = store);

  }

  deliver(): void {
    this.orderService.deliverOrder(String(this.order.id)).subscribe(res => this.router.navigate(['/partner/orders']));
  }

  distanceToShop(): string {
    const res = distance(this.order, this.store);
    if (isNaN(res)) {
      return 'Местоположение неопределено';
    } else {
      return res.toFixed(2) + ' км.';
    }
  }
}
