import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {generateErrorMessage} from '../../../root/services/utils';
import {PartnerStoreService} from '../../services/store.service';
import {Store} from '../../../models/Store';
import {ModalService} from '../../../shared/modal.service';

@Component({
  selector: 'app-shop-info',
  templateUrl: './shop-info.component.html',
  styleUrls: ['./shop-info.component.scss']
})
export class ShopInfoComponent implements OnInit {
  userSubscription;
  shopForm: FormGroup;

  constructor(private partnerStoreService: PartnerStoreService,
              private router: Router,
              public modalService: ModalService,
              private route: ActivatedRoute) {
  }

  shopInfoFieldGroup = [
    {
      name: 'description',
      plainName: 'Описание(будет использоваться как заголовок на карте)',
      placeholder: 'Описание',
      validators: [Validators.required, Validators.minLength(7)],
      inputType: 'text',
      disabled: false,
      default: ''
    },
    {
      name: 'phone',
      plainName: 'телефон',
      placeholder: 'Телефон',
      validators: [Validators.required, Validators.pattern('^((\\+7|7|8)+([0-9]){10})$')],
      errorText: (errors) => {
        if (errors.pattern) {
          return 'Ожидаемый формат +71234567890'
        }
      },
      inputType: 'telephone',
      disabled: false,
      default: ''
    },
    {
      name: 'latitude',
      plainName: 'Широта',
      placeholder: 'Широта',
      validators: [Validators.required],
      inputType: 'number',
      disabled: false,
      default: ''
    },
    {
      name: 'longitude',
      plainName: 'Долгота',
      placeholder: 'Долгота',
      validators: [Validators.required],
      inputType: 'number',
      disabled: false,
      default: ''
    }
  ];

  ngAfterViewInit(): void {
    let first = this.route.snapshot.queryParamMap.get('first');
    if (first) {
      this.modalService.open('first-shop')
    }
  }

  ngOnInit() {
    this.userSubscription = this.partnerStoreService.getAdminStore().subscribe((shop: Store) => {
        if (shop !== null) {
          this.shopInfoFieldGroup.map((field) => {
            field.default = shop[field.name];
            return field;
          });
          this.shopForm = new FormGroup(
            this.shopInfoFieldGroup.reduce((form, field) => {
              form[field.name] = new FormControl({value: field.default, disabled: field.disabled}, field.validators);
              return form;

            }, {})
          );
        } else {
          this.router.navigate(['/login']);
        }
    });
  }

  save() {
    if (this.shopForm.valid) {
      let result = <Store>this.shopInfoFieldGroup.reduce((result, field) => {
        if (!field.disabled && this.shopForm.get(field.name).value) {
          result[field.name] = this.shopForm.get(field.name).value;
        }
        return result;
      }, {});
      this.partnerStoreService.patchAdminStore(result).subscribe((errors) => {
        console.log(errors)
      })
    } else {
      this.shopForm.markAllAsTouched()
    }
  }

  generateErrorMessage(errors, field) {
    return generateErrorMessage(errors, field)
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }

}
