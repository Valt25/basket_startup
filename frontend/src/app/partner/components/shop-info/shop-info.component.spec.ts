import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ShopInfoComponent} from './shop-info.component';
import {MaterialModule} from '../../../material.module';
import {ReactiveFormsModule} from '@angular/forms';
import {Component} from '@angular/core';
import {PartnerStoreService} from '../../services/store.service';
import {RouterTestingModule} from '@angular/router/testing';
import {Observable, of} from 'rxjs';
import {Store} from '../../../models/Store';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

@Component({selector: 'app-modal', template: ''})
class ModalStubComponent {}

describe('ShopInfoComponent', () => {
  let component: ShopInfoComponent;
  let fixture: ComponentFixture<ShopInfoComponent>;
  let storeStubService: Partial<PartnerStoreService>;

    storeStubService = {
      getAdminStore(): Observable<Store> {
        return of(new Store())
      }
    };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopInfoComponent, ModalStubComponent ],
      imports: [
        MaterialModule,
        ReactiveFormsModule,
        RouterTestingModule.withRoutes([]),
        NoopAnimationsModule
      ],
      providers: [
        {provide: PartnerStoreService, useValue: storeStubService},

      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
