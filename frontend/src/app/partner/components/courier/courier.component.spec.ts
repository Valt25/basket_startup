import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CourierComponent} from './courier.component';
import {RouterTestingModule} from '@angular/router/testing';
import {PartnerStoreService} from '../../services/store.service';
import {PartnerOrderService} from '../../services/partner-order.service';
import {WebsocketService} from '../../../root/websocket/websocket.service';
import {Observable, of} from 'rxjs';
import {testOrder} from '../../../client/components/payment/payment.component.spec';
import {Order} from '../../../models/Order';
import {Store} from '../../../models/Store';
import {WebSocketConfig} from '../../../root/websocket/websocket.interfaces';

describe('CourierComponent', () => {
  let component: CourierComponent;
  let fixture: ComponentFixture<CourierComponent>;
  let storeStubService: Partial<PartnerStoreService>;
  let orderStubService: Partial<PartnerOrderService>;
  let wsStubService: Partial<WebsocketService>;

  orderStubService = {
    getAvailableOrders(): Observable<Order[]> {
      return of([testOrder])
    }
  };

  storeStubService = {
    getAdminStore(): Observable<Store> {
      return of(new Store())
    }
  };

  wsStubService = {
    init(wsConfig: WebSocketConfig) {
    },
    on(): Observable<any> {
      return of({})
    }
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourierComponent ],
      imports: [RouterTestingModule.withRoutes([])],
      providers: [
        {provide: PartnerStoreService, useValue: storeStubService},
        {provide: PartnerOrderService, useValue: orderStubService},
        {provide: WebsocketService, useValue: wsStubService},
      ]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
