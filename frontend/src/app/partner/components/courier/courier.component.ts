import { Component, OnInit } from '@angular/core';
import {Order} from '../../../models/Order';
import {Store} from '../../../models/Store';
import {PartnerStoreService} from '../../services/store.service';
import {PartnerOrderService} from '../../services/partner-order.service';
import {WebsocketService} from '../../../root/websocket/websocket.service';
import {distance, getWsProtocol} from '../../../root/services/utils';

@Component({
  selector: 'app-courier',
  templateUrl: './courier.component.html',
  styleUrls: ['./courier.component.scss']
})
export class CourierComponent implements OnInit {
  orders: Order[];
  store: Store;


  constructor(private orderService: PartnerOrderService,
              private storeService: PartnerStoreService,
              private wsService: WebsocketService) { }

  ngOnInit() {
    this.orderService.getAvailableOrders().subscribe(orders => {
      this.orders = orders;
    });
    this.storeService.getAdminStore().subscribe((store: Store) => this.store = store);
    this.wsService.init({url: getWsProtocol() + window.location.host + '/ws/partner/'});

    this.wsService.on().subscribe((res: any) => {
      const order_id = res.order_id;
      const currentOrder = this.orders.find((element, index, array) => element.id === order_id);
      if (currentOrder !== undefined) {
        currentOrder.latitude = res.location.latitude;
        currentOrder.longitude = res.location.longitude;
      }
    });
  }


  distanceToShop(order: Order): string {
    const res = distance(order, this.store);
    if (isNaN(res)) {
      return 'Местоположение неопределено';
    } else {
      return res.toFixed(2) + ' км.';
    }
  }



}
