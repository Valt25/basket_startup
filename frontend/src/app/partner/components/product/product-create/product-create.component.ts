import { Component, OnInit } from '@angular/core';
import {PartnerProductService} from '../../../services/partner-product.service';
import {Router} from '@angular/router';
import {Product} from '../../../../models/Product';

@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.css']
})
export class ProductCreateComponent implements OnInit {
  product: Product;

  constructor(private productService: PartnerProductService,
              private router: Router) { }

  ngOnInit() {
    this.product = new Product();
  }

  onSubmit(product: Product): void {
    this.productService.createProduct(product).subscribe(res => this.router.navigate(['/partner/menu']));
  }

}
