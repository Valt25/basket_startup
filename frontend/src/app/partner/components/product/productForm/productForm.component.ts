import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PartnerProductService} from '../../../services/partner-product.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Product} from '../../../../models/Product';

@Component({
  selector: 'app-product-form',
  templateUrl: './productForm.component.html',
  styleUrls: ['./productForm.component.css']
})

export class ProductFormComponent implements OnInit {

  @Output() submit = new EventEmitter<Product>();
  @Input() product: Product;

  productForm: FormGroup;

  ngOnInit() {

    this.productForm = new FormGroup({
      'name': new FormControl(this.product.name, [Validators.required, Validators.minLength(3)]),
      'description': new FormControl(this.product.description, [Validators.minLength(10)]),
      'price': new FormControl(this.product.price, [Validators.required, Validators.min(0)]),
      'photo': new FormControl(this.product.photo, [Validators.required]),
      'category': new FormControl(this.product.category, [Validators.required, Validators.minLength(3)])
    });
  }

  constructor(private route: ActivatedRoute,
              private productService: PartnerProductService,
              private cd: ChangeDetectorRef) {
  }

  onFileChange(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.productForm.patchValue({
          photo: reader.result
        });
        // need to run CD since file load runs outside of zone
        this.cd.markForCheck();
      };
    }
  }

  get name() {
    return this.productForm.get('name');
  }

  get description() {
    return this.productForm.get('description');
  }

  get price() {
    return this.productForm.get('price');
  }

  get photo() {
    return this.productForm.get('photo');
  }

  get category() {
    return this.productForm.get('category');
  }

  submitForm(): void {
    if (this.productForm.valid) {
      this.submit.emit({
        id: this.product.id,
        name: this.productForm.get('name').value,
        description: this.productForm.get('description').value,
        price: this.productForm.get('price').value,
        photo: this.productForm.get('photo').value,
        category: this.productForm.get('category').value,
      });
    }
  }
}
