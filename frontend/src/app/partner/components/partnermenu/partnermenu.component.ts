import {Component, OnInit} from '@angular/core';
import {Store} from '../../../models/Store';
import {Product} from '../../../models/Product';
import {PartnerStoreService} from '../../services/store.service';
import {PartnerProductService} from '../../services/partner-product.service';

@Component({
  templateUrl: './partnermenu.component.html',
  styleUrls: ['./partnermenu.component.css']
})
export class PartnerMenuComponent implements OnInit {
  store: Store;

  constructor(private storeService: PartnerStoreService,
              private productService: PartnerProductService) {

  }

  ngOnInit() {
    this.storeService.getAdminStore().subscribe((store: Store) => this.store = store);

  }

  deleteProduct(product: Product) {
    this.store.products.splice(this.store.products.indexOf(product, 0), 1);
    this.productService.deleteProduct(product.id);
  }




}
