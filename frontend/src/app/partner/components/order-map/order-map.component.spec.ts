import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrderMapComponent} from './order-map.component';
import {PartnerOrderService} from '../../services/partner-order.service';
import {WebsocketService} from '../../../root/websocket/websocket.service';
import {UtilityService} from '../../../shared/utility.service';
import {Observable, of} from 'rxjs';

describe('OrderMapComponent', () => {
  let component: OrderMapComponent;
  let fixture: ComponentFixture<OrderMapComponent>;
  let orderStubService: Partial<PartnerOrderService>;
  let wsStubService: Partial<WebsocketService>;
  let utilityService: Partial<UtilityService>;

  utilityService = {
    loadScript(scriptSource: string, objectName: string): Observable<any> {
      return of({ready: () => {}})
    }
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderMapComponent ],
      providers: [
        {provide: PartnerOrderService, useValue: orderStubService},
        {provide: WebsocketService, useValue: wsStubService},
        {provide: UtilityService, useValue: utilityService}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
