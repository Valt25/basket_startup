import {Component, OnInit} from '@angular/core';
import {zip} from 'rxjs';
import {LocatedOrder, Location} from '../../../models/LocatedOrder';
import {PartnerOrderService} from '../../services/partner-order.service';
import {WebsocketService} from '../../../root/websocket/websocket.service';
import {UtilityService} from '../../../shared/utility.service';

class LocatedOrderWithPlacemark extends LocatedOrder {
  placemark: any;
}

@Component({
  selector: 'app-order-map',
  templateUrl: './order-map.component.html',
  styleUrls: ['../../../root/styles/map.styles.scss', './order-map.component.scss']
})
export class OrderMapComponent implements OnInit {
  private ymaps: any;
  private myMap: any;
  private lock = false;
  public orders: LocatedOrderWithPlacemark[] = [];

  constructor(private wsService: WebsocketService,
              private orderService: PartnerOrderService,
              private utilityService: UtilityService) {
  }

  ngOnInit() {
    const ymaps$ = this.utilityService.loadScript('https://api-maps.yandex.ru/2.1/?lang=ru_RU', 'ymaps');
    zip(ymaps$).subscribe(([ymaps]) => {
      this.ymaps = ymaps;
      // this.orders = orders.map(order => new LocatedOrder(order, null));
      ymaps.ready(() => {
        // Создание экземпляра карты и его привязка к контейнеру с
        // заданным id ("map").
        const myMap = new ymaps.Map('map', {
          // При инициализации карты обязательно нужно указать
          // её центр и коэффициент масштабирования.
          center: [55.76, 37.64], // Москва
          zoom: 10,
          controls: [new ymaps.control.GeolocationControl(), 'zoomControl']
        });
        this.myMap = myMap;
        this.wsService.init({url: 'wss://' + window.location.host + '/ws/partner/'});
        this.wsService.on().subscribe((res: any) => {
          const order_id = res.order_id;
          this.lock = true;
          const currentOrder = this.orders.find((element, index, array) => element.order.id === order_id);
          if (currentOrder === undefined) {
            this.orderService.getOrder(order_id).subscribe((order) => {
              if (this.orders.find((element, index, array) => element.order.id === order_id) === undefined) {
                const newOrder = new LocatedOrderWithPlacemark(order, new Location(res.location.latitude, res.location.longitude));
                this.orders.push(newOrder);
                this.drowOrder(newOrder);

              }
            });
          } else {
            currentOrder.location = new Location(res.location.latitude, res.location.longitude);
            this.drowOrder(currentOrder);
          }
        });
      });


    });
  }

  drowOrder(order: LocatedOrderWithPlacemark): void {
    if (order.location) {
      if (order.placemark) {
        this.myMap.geoObjects.remove(order.placemark);
      }
      const newPlacemark = new this.ymaps.Placemark([order.location.lattitude, order.location.longitude], {
        balloonContentHeader: order.order.client.name_surname,
        hintContent: order.order.client.name_surname
      });
      order.placemark = newPlacemark;

      this.myMap.geoObjects.add(newPlacemark);
      // this.drawedStores.push(newPlacemark);
    }

  }

}
