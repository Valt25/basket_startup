import {inject, TestBed} from '@angular/core/testing';

import {PartnerProductService} from './partner-product.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('PartnerProductService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PartnerProductService],
      imports: [HttpClientTestingModule]
    });
  });

  it('should be created', inject([PartnerProductService], (service: PartnerProductService) => {
    expect(service).toBeTruthy();
  }));
});
