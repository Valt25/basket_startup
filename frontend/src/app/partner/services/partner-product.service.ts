import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Product} from '../../models/Product';

@Injectable({
  providedIn: 'root'
})
export class PartnerProductService {

  constructor(private http: HttpClient) {
  }

  deleteProduct(id: number): void {
    this.http.delete(`api/map/admin/product/${id}/`).subscribe(res => console.log(res));
  }

  getProduct(id: string): Observable<Product> {
    return this.http.get<Product>(`api/map/products/${id}/`);
  }

  editProduct(product: any): Observable<any> {
    return this.http.patch(`api/map/admin/product/${product.id}/`, product);
  }

  createProduct(product: Product): Observable<any>{
    return this.http.post('api/map/admin/product/', product);
  }
}
