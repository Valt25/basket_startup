import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/internal/operators';
import {Order} from '../../models/Order';

@Injectable({
  providedIn: 'root'
})
export class PartnerOrderService {

  constructor(private http: HttpClient) {
  }

  getAvailableOrders(): Observable<Order[]> {
    return this.http.get<Order[]>('api/orders/shop/').pipe(
      map((plain_arr: any[]) => plain_arr.map((plain: any) => new Order(plain.id,
        plain.shop_id,
        plain.created,
        plain.delivered,
        plain.arrived,
        plain.payed,
        plain.products,
        plain.client,
        plain.latitude,
        plain.longitude)))
    );
  }

  getOrder(id: string): Observable<Order> {
    return this.http.get<Order>(`api/orders/shop/${id}`).pipe(
      map((plain: any) => new Order(plain.id, plain.shop_id, plain.created, plain.delivered, plain.arrived, plain.payed, plain.products, plain.client, plain.latitude, plain.longitude)));
  }

  deliverOrder(id: string): Observable<any> {
    return this.http.post<Order>(`api/orders/shop/${id}/deliver`, {});
  }
}

