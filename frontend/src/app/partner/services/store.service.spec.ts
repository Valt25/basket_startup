import {inject, TestBed} from '@angular/core/testing';

import {PartnerStoreService} from './store.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('PartnerStoreService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PartnerStoreService],
      imports: [HttpClientTestingModule]
    });
  });

  it('should be created', inject([PartnerStoreService], (service: PartnerStoreService) => {
    expect(service).toBeTruthy();
  }));
});
