import {inject, TestBed} from '@angular/core/testing';

import {PartnerOrderService} from './partner-order.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('PartnerOrderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PartnerOrderService],
      imports: [HttpClientTestingModule]
    });
  });

  it('should be created', inject([PartnerOrderService], (service: PartnerOrderService) => {
    expect(service).toBeTruthy();
  }));
});
