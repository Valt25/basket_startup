import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Store} from '../../models/Store';

@Injectable({
  providedIn: 'root'
})
export class PartnerStoreService {

  constructor(private http: HttpClient) {
  }


  getAdminStore(): Observable<Store> {
    return this.http.get<Store>(`api/map/admin/shop/`);
  }

  patchAdminStore(newStore: Store): Observable<Store> {
    return this.http.patch<Store>(`api/map/admin/shop/`, newStore);
  }

}
