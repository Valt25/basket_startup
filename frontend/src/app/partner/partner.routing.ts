import {RouterModule, Routes} from '@angular/router';
import {PartnerMenuComponent} from './components/partnermenu/partnermenu.component';
import {PartnerOrdersComponent} from './components/partnerOrders/partnerOrders.component';
import {OrderComponent} from './components/order/order.component';
import {OrderMapComponent} from './components/order-map/order-map.component';
import {CourierComponent} from './components/courier/courier.component';
import {ProductEditComponent} from './components/product/product-edit/product-edit.component';
import {ProductCreateComponent} from './components/product/product-create/product-create.component';
import {NgModule} from '@angular/core';
import {AdminLayoutComponent} from './layout/admin-layout/admin-layout.component';
import {ShopInfoComponent} from './components/shop-info/shop-info.component';

export const routes: Routes = [
  {
    path: '', component: AdminLayoutComponent,
    children: [
      {path: 'menu', component: PartnerMenuComponent},
      {path: 'orders', component: PartnerOrdersComponent},
      {path: 'order/:id', component: OrderComponent},
      {path: 'courier', component: CourierComponent},
      {path: 'product/edit/:id', component: ProductEditComponent},
      {path: 'product/create', component: ProductCreateComponent},
      {path: 'shop/info', component: ShopInfoComponent},
      {path: 'map', component: OrderMapComponent},
      {path: '', redirectTo: 'shop/info', pathMatch: 'full'},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PartnerRoutingModule {
}

