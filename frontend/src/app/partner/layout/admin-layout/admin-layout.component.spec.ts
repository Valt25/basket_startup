import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AdminLayoutComponent} from './admin-layout.component';
import {Component} from '@angular/core';
import {RouterTestingModule} from '@angular/router/testing';

@Component({selector: 'app-navbar', template: ''})
class NavbarStubComponent {}

describe('AdminLayoutComponent', () => {
  let component: AdminLayoutComponent;
  let fixture: ComponentFixture<AdminLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminLayoutComponent, NavbarStubComponent ],
      imports: [RouterTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
