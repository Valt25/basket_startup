import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './routing.module';
import {AppComponent} from './app.component';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {BugCollectorComponent} from './root/components/bug-collector/bug-collector.component';
import {ErrorInterceptor} from './root/services/auth/error.request';
import {WebsocketModule} from './root/websocket/websocket.module';
import {MatMenuModule} from '@angular/material';
import {UserProfileComponent} from './root/components/user-profile/user-profile.component';
import {AuthService} from './root/services/auth/auth.service';
import {MaterialModule} from './material.module';
import {BecomePartnerComponent} from './root/components/become-partner/become-partner.component';
import {NotFoundComponent} from './root/components/not-found/not-found.component';
import {SharedModule} from './shared/shared.module';


@NgModule({
  declarations: [
    AppComponent,
    BugCollectorComponent,
    UserProfileComponent,
    BecomePartnerComponent,
    NotFoundComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    WebsocketModule.config({
      url: 'ws' + window.location.hostname + '/ws/'
    }),
    MatMenuModule,
    SharedModule,

  ],
  providers: [
    HttpClient,
    AuthService,

    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    // { provide: ErrorHandler, useClass: SentryErrorHandler }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
