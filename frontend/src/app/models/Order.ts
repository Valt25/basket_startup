import {ProductToBuy} from './ProductToBuy';
import {Profile} from './Profile';

export class Order {


  constructor(id: number, shop_id: number, created: string, delivered: boolean, arrived: boolean, payed: boolean, products: ProductToBuy[], profile: Profile, latitude: number, longitude: number) {
    this.id = id;
    this.shop_id = shop_id;
    this.created = created;
    this.delivered = delivered;
    this.arrived = arrived;

    this.payed = payed;
    this.products = products;
    this.client = profile;
    this.latitude = latitude;
    this.longitude = longitude;
  }


  id: number;
  shop_id: number;
  created: string;
  delivered: boolean;
  arrived: boolean;

  payed: boolean;
  products: ProductToBuy[];
  client: Profile;

  latitude: number;
  longitude: number;

  public totalPrice(): number {
    let result = 0;
    for (const product of this.products) {
      result += product.product.price * product.amount;
    }
    return result;
  }
}
