import {Product} from './Product';

export class ProductToBuy {
  product: Product;
  amount: number;

  constructor(product: any, amount: number) {
    if (product instanceof Product) {
      this.product = product;
    } else {
      this.product = new Product(product);
    }
    this.product = product;
    this.amount = amount;
  }

  getCost(): number {
    return this.product.price * this.amount;
  }
}
