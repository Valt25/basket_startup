export class Product {
  id: number;
  name: string;
  description: string;
  category: string;
  price: number;
  photo: string;


  constructor(productObject?: any) {
    if (productObject) {
      this.id = productObject.id;
      this.name = productObject.name;
      this.description = productObject.description;
      this.category = productObject.category;
      this.price = productObject.price;
      this.photo = productObject.photo;
    }
  }
}
