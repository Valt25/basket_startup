export class BugReport {
  title: string;
  contact: string;
  description: string;
  image: any;


  constructor(title: string, contact: string, description: string, image: any) {
    this.title = title;
    this.contact = contact;
    this.description = description;
    this.image = image;
  }
}
