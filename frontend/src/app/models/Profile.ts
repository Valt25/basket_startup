import {ProductToBuy} from './ProductToBuy';

export class Profile {
  username: string;
  name_surname: string;
  email: string;
  car_description: string;
  car_number: string;
  phone: string;


}
