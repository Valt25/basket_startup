export class  User {

  constructor() {
    this.username = '';
    this.password = '';
  }

  username: string;
  password: string;
}


export class AppUser {

  username: string;
  name_surname: string;
  email: string;
  car_description: string;
  car_number: string;
  phone: string;
  shop: number;

  constructor(data) {
    this.username = data.username;
    this.name_surname = data.name_surname;
    this.email = data.email;
    this.car_description = data.car_description;
    this.car_number = data.car_number;
    this.phone = data.phone;
    this.shop = data.shop;
  }
}

export type RegisterUser = AppUser | User;
