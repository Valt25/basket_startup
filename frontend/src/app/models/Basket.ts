import {ProductToBuy} from './ProductToBuy';

export class Basket {
  id: number;
  shop: number;
  products: ProductToBuy[];

  constructor(shop: number, products: ProductToBuy[]) {
    this.id = this.shop = shop;
    this.products = products;
  }

  getTotalPrice(): number {
    let result = 0;
    for (const product of this.products) {
      result += product.amount * product.product.price;
    }
    return result;
  }
}
