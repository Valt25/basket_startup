import {Order} from './Order';

export class Location {

  constructor(lattitude: number, longitude: number) {
    this.lattitude = lattitude;
    this.longitude = longitude;
  }

  lattitude: number;
  longitude: number;
}

export class LocatedOrder {


  constructor(order: Order, location: Location) {
    this.order = order;
    this.location = location;
  }

  order: Order;
  location: Location;

}
