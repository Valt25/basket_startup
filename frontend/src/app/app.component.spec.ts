import {async, TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {MaterialModule} from './material.module';
import {RouterTestingModule} from '@angular/router/testing';
import {Component} from '@angular/core';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

@Component({selector: 'app-user-profile', template: '', inputs: ['is_in_sidenav']})
class UserProfileStubComponent {}

@Component({selector: 'app-become-partner', template: '', inputs: ['is_in_sidenav']})
class BecomePartnerStubComponent {}

@Component({selector: 'app-bug-collector', template: '', inputs: ['is_in_sidenav']})
class BugCollectorStubComponent {}

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        UserProfileStubComponent,
        BecomePartnerStubComponent,
        BugCollectorStubComponent
      ],
      imports: [MaterialModule, RouterTestingModule, NoopAnimationsModule]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  // it(`should have as title 'app'`, async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   const app = fixture.debugElement.componentInstance;
  //   expect(app.title).toEqual('app');
  // }));
  // it('should render name in a h1 tag', async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;
  //   expect(compiled.querySelector('h1').textContent).toContain('Welcome to app!');
  // }));
});
